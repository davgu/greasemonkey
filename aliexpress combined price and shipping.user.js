// ==UserScript==
// @name         aliexpress combined price and shipping
// @namespace    http://tampermonkey.net/
// @version      1
// @description  Add price with shipping
// @match        https://www.aliexpress.com/item/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=aliexpress.com
// @grant        none
// ==/UserScript==

var price = document.querySelector('.product-price')
var currentPrice = document.querySelector('.product-price-current')

var combined = document.createElement('div')
combined.textContent = `Price + shipping: `
price.append(combined)


function combinePrice() {
  var fPrice = parseFloat(price.innerText.match(/\d+,\d+/)[0].replace(',', '.'))
  var shipping = document.querySelector('.dynamic-shipping strong')
  var shippingPrice = 0
  if (!/free/i.test(shipping.innerText)) {
      shippingPrice = parseFloat(shipping.innerText.match(/\d+,\d+/)[0].replace(',', '.'))
  }
  combined.textContent = `Price + shipping: ${(fPrice + shippingPrice).toFixed(2)} kr`.replace('.', ',')
}

var observer = new MutationObserver(combinePrice);
observer.observe(currentPrice, {
  characterData: true,
  subtree: true
});

combinePrice()