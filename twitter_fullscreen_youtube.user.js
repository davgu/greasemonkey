// ==UserScript==
// @name        twitter fullscreen youtube
// @namespace   none
// @description Allow fullscreen videos on twitter
// @include     https://twitter.com/i/cards/*
// @version     1
// @grant       none
// ==/UserScript==

document.querySelectorAll('iframe[data-src*="youtube.com/embed"]').forEach(f => {
	f.allowFullscreen = true;
});
