// ==UserScript==
// @name        reddit subreddit dropdown
// @namespace   reddit
// @description Enables dropdown menu with subreddits on reddit with subreddit top bar set to fixed in RES
// @include     https://www.reddit.com/*
// @version     2
// @grant       none
// ==/UserScript==

var header = document.getElementById('sr-header-area');
header.style.overflow = 'visible';
header.querySelector('.drop-choices').style.overflowY = 'auto';