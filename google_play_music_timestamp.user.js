// ==UserScript==
// @name         Google play music show timestamp
// @namespace    play.google.com
// @version      1
// @description  Shows timestamp without hover
// @author       David
// @match        https://play.google.com/music/listen?*
// @grant        none
// ==/UserScript==

var myStyle = document.createElement('style');
myStyle.innerHTML = `
#time_container_current, #time_container_duration, .time-label {
  opacity: 1 !important;
}
`
document.head.appendChild(myStyle);
