// ==UserScript==
// @name        aftonbladet sportbladet remove blur filter
// @namespace   aftonbladet
// @description Removes the blur filter on artices on aftonbladet due to adblock
// @include     https://www.aftonbladet.se/sportbladet/*
// @version     1
// @grant       none
// ==/UserScript==

document.querySelector('.qVgIk').classList.remove('qVgIk');
