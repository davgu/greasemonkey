// ==UserScript==
// @name         reddit preview youtube title
// @namespace    http://reddit.com
// @version      1.1
// @description  Fetch and display youtube video titles when hovering a link
// @match        https://www.reddit.com/*
// @match        https://old.reddit.com/*
// @icon         https://www.google.com/s2/favicons?domain=reddit.com
// @grant        none
// ==/UserScript==

var ytLinks = document.querySelectorAll('a[href*="youtube.com"], a[href*="youtu.be"]');

for (var l of ytLinks) {
    l.addEventListener('mouseover', addDetails);
}

async function addDetails(e) {
    var video = e.target;
    var ytUrl = `https://www.youtube.com/oembed?url=${video.href}&format=json`
    var r = await fetch(ytUrl);
    var j = await r.json();
    video.title = `${j.title}\nChannel: ${j.author_name}`;
    video.removeEventListener("mouseover", addDetails);
}
