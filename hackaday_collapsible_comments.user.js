// ==UserScript==
// @name         hackaday collapsible comments
// @namespace    https://hackaday.com/
// @version      1
// @description  Add a button on comments to collapse their children
// @match        https://hackaday.com/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=hackaday.com
// @grant        none
// ==/UserScript==

var style = document.createElement('style')
style.innerHTML = `
.hidden {
display: none !important;
}
`
document.head.append(style)


document.querySelectorAll('ol.comment-list li').forEach(comment => {
  var abuse = comment.querySelector('.report-abuse')
  var collapseSpan = document.createElement('span')
  var collapseTarget = document.createElement('a')
  collapseTarget.href = '#'
  collapseTarget.text = 'Collapse'
  collapseTarget.addEventListener('click', collapseListener)
  collapseSpan.append(collapseTarget)
  collapseSpan.style.marginLeft = '1em'
  abuse.append(collapseSpan)
})


function collapseListener(e) {
  e.preventDefault()
  var li = e.target.closest('li')
  li.querySelector('ol').classList.toggle('hidden')
  e.target.text = e.target.text == 'Collapse' ? 'Show' : 'Collapse'
}