// ==UserScript==
// @name        instagram add full size link
// @namespace   instagram
// @description Adds link to full size image
// @include     https://www.instagram.com/p/*
// @version     1
// @grant       none
// ==/UserScript==

// var img = document.querySelector('img[alt]');
var img = document.querySelectorAll('img[src*="cdninstagram.com"]')[1];
var article = document.querySelector('article');
var a = document.createElement('a');
a.text = 'Full size';
a.href = img.src;
article.appendChild(a);