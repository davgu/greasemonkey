// ==UserScript==
// @name        corren stop paywall
// @namespace   none
// @description Stops corren from checking if article is behind paywall
// @include     http*://corren.se/*
// @include     http*://www.corren.se/*
// @include     http*://www.affarsliv.com/*
// @include     http*://affarsliv.com/*
// @version     1.4
// @grant       none
// ==/UserScript==

if (Paywall) Paywall = null;

