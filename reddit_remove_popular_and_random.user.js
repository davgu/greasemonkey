// ==UserScript==
// @name        reddit remove menu items
// @namespace   reddit
// @description Removes menu items from subreddit bar
// @include     https://www.reddit.com/*
// @version     2
// @grant       none
// ==/UserScript==

var noThanks = ['popular', 'users', 'random'];
var menuItems = document.querySelector('ul.sr-bar');
var choices = menuItems.querySelectorAll('.choice');
for (var choice of choices) {
  if (noThanks.includes(choice.textContent)) {
    menuItems.removeChild(choice.parentNode);
  }
}