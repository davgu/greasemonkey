// ==UserScript==
// @name        geekbench add diff
// @namespace   none
// @description Add column of differences while comparing cpus
// @include     https://browser.geekbench.com/v4/cpu/compare/*
// @version     1
// @grant       none
// ==/UserScript==

var rows = document.querySelectorAll('tr');
for (var row of rows) {
  var scores = row.querySelectorAll('.score');
  if (scores.length < 1) continue;
  var score2 = parseInt(scores[0].textContent);
  var score1 = parseInt(scores[1].textContent);
  var diff = score1 - score2;
  var diffPercent = (diff/score2) * 100;
  var tdDiff = document.createElement('td');
  tdDiff.textContent = diff;
  row.appendChild(tdDiff);
  var tdDiffPercent = document.createElement('td');
  tdDiffPercent.textContent = diffPercent.toFixed(1) + ' %';
  tdDiffPercent.style.whiteSpace = 'nowrap';
  if (diffPercent < 0) tdDiffPercent.style.color = 'red';
  row.appendChild(tdDiffPercent);
}