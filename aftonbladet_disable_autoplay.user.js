// ==UserScript==
// @name        aftonbladet disable autoplay
// @namespace   none
// @description Disables autoplay on videos in articles
// @include     http://www.aftonbladet.se/*
// @version     1
// @grant       none
// ==/UserScript==

document.querySelectorAll('iframe').forEach(f => {
  f.src = f.src.replace('autoplay=true', 'autoplay=false');
});