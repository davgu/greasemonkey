// ==UserScript==
// @name         aftonbladet remove filter: blur
// @namespace    aftonbladet
// @version      0.1
// @description  Remove the blur filter aftonbladet puts over articles because of adblocking
// @author       You
// @match        https://www.aftonbladet.se/*
// @grant        none
// ==/UserScript==

var cssText = `
.qVgIk {
  webkit-filter: unset;
  filter: unset;
}

._3WxSp {
  display: none;
}
`;

var style = document.createElement('style');
style.innerText = cssText;
document.getElementsByTagName('head')[0].appendChild(style);