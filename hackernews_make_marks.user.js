// ==UserScript==
// @name         hackernews mark quotes and op
// @namespace    https://news.ycombinator.com
// @version      3.4
// @description  Show what paragraphs in comments are quotes and who is op
// @match        https://news.ycombinator.com/item?id=*
// @grant        none
// ==/UserScript==

var style = document.createElement('style');
style.innerHTML = `
.quote-comment {
  background-color: #FCE1BD;
  margin-top: 0 !important;
  padding: 3px 10px 3px 0.75em;
  font-style: italic;
  border-left: 10px solid #db7113;
  display: inline-block;
  border-radius: 5px;
}

  .hnuser-op {
    color: #fff!important;
    background: #276fed;
    border-radius: 4px;
    padding: 2px;
}
`;
document.head.appendChild(style);

var comments = document.querySelectorAll('div.comment > span');


for (var comment of comments) {
  var skip = false;
  for (var child of comment.childNodes) {
    if (child.nodeName == 'A') { // child node is a link
      skip = true;
      continue;
    }
    if (child.textContent[0] === '>') { // quote, maybe
      if (skip) { // previous node was a link, this is not a quote :(
        skip = false;
        continue;
      }
      var quoteText = child.textContent;
      if (child.textContent.trim().length <= 1) {
        quoteText += child.nextSibling.textContent;
        child.parentNode.removeChild(child.nextSibling);
      }
      var p = document.createElement('p');
      p.textContent = quoteText;
      comment.insertBefore(p, child);
      comment.removeChild(child);
      p.className = 'quote-comment';
    }
  }
}

// mark op
var op = document.querySelector('a.hnuser');
[...document.querySelectorAll('a.hnuser')]
  .filter(user => user.textContent === op.textContent)
  .forEach(user => { user.classList.add('hnuser-op') });
