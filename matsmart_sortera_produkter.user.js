// ==UserScript==
// @name        matsmart sortera produkter
// @namespace   matsmart
// @description Sortera produkter efter pris eller rabatt
// @include     https://www.matsmart.se/*
// @version     1
// @grant       none
// ==/UserScript==

var content = document.querySelector('.view-content');
var products = [...content.querySelectorAll('div.prd')];

var priceButton = document.createElement('a');
priceButton.text = 'Billigast först!';
priceButton.addEventListener('click', sortByPrice, false);
priceButton.href = '#';
priceButton.style = 'padding-left: 5px; padding-right: 5px';

var discountButton = document.createElement('a');
discountButton.text = 'Mest rabatt först!';
discountButton.addEventListener('click', sortByDiscount, false);
discountButton.href = '#';
discountButton.style = 'padding-left: 5px; padding-right: 5px';

var buttons = document.createElement('span');
buttons.style.float = 'right';
buttons.appendChild(priceButton);
buttons.appendChild(discountButton);

var contentDiv = document.getElementById('content');
var title = contentDiv.querySelector('.wrap-title-black');
title.appendChild(buttons);


function sortByPrice(e) {
  e.preventDefault();
  var content = document.querySelector('.view-content');
  var products = [...content.querySelectorAll('div.prd')];
  products.sort(comparePrice);
  [...content.children].forEach(c => c.remove())
  products.forEach(p => content.appendChild(p));
}

function sortByDiscount(e) {
  e.preventDefault();
  var content = document.querySelector('.view-content');
  var products = [...content.querySelectorAll('div.prd')];
  products.sort(compareDiscount);
  [...content.children].forEach(c => c.remove())
  products.forEach(p => content.appendChild(p));
}

function getPrice(product) {
  return parseInt(product.querySelector('.prd-price-num').textContent);
}

function getDiscount(product) {
  var discount = product.querySelector('span.prd-discount-oldprice > span');
  if (!discount) return 0;
  return discount ? parseInt(discount.textContent.match(/\(-(\d+)\%\)/)[1]) : 0
}

function comparePrice(a, b) {
  return getPrice(a) - getPrice(b);
}

function compareDiscount(a, b) {
  return getDiscount(b) - getDiscount(a);
}