// ==UserScript==
// @name         linköping snygga till kartan
// @namespace    kartan.linkoping.se
// @version      1
// @description  Lägger lite filter på kartan så den inte ser så tråkig ut
// @author       David
// @match        http://kartan.linkoping.se/*
// @grant        none
// @run-at       document-idle
// ==/UserScript==

var style = document.createElement('style');
style.innerText = `
.enhanced {
  filter: saturate(200%) contrast(110%) brightness(120%);
}
`;
document.getElementsByTagName('head')[0].appendChild(style);

addButtonListeners();

async function addButtonListeners() {
  var buttons;
  do { // keep trying until they are loaded
    buttons = document.querySelectorAll('.mmiconstate');
    await sleep(500);
  } while (buttons.length < 1);

  buttons[0].addEventListener('click', e => {
    document.querySelector('canvas').classList.remove('enhanced');
  });
  buttons[1].addEventListener('click', e => {
    document.querySelector('canvas').classList.add('enhanced');
  });
  buttons[2].addEventListener('click', e => {
    document.querySelector('canvas').classList.remove('enhanced');
  });
}

// https://stackoverflow.com/a/39914235
function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}
