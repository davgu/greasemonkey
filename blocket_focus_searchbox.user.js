// ==UserScript==
// @name         blocket focus search field
// @version      1
// @description  Focus search box to make it possible to type directly
// @match        https://www.blocket.se
// @grant        none
// ==/UserScript==

var autosuggest = document.getElementById('react-autowhatever-search-input');
autosuggest.style.display = 'none';
var search = document.querySelector('input.react-autosuggest__input');
search.focus();

search.addEventListener('keydown', showSuggestionBox);

function showSuggestionBox(e) {
    search.removeEventListener('keydown', showSuggestionBox);
    autosuggest.style.display = 'unset';
}
