// ==UserScript==
// @name         flickr load album fullsize
// @namespace    flickr.com
// @version      2
// @description  Load largest verion of images in album
// @match        https://www.flickr.com/photos/*/sets/*
// @match        https://www.flickr.com/photos/*/albums/*
// @grant        none
// ==/UserScript==

async function fetchUrls() {
    var button = document.getElementById('dlButton');
    button.textContent = '⏳';
    button.className = '';

  var photos = document.querySelectorAll('.photo-list-photo-view');
  for (var photo of photos) {
    dlPromises.push(addFullSizeUrl(photo));
  }

    Promise.all(dlPromises).then(() => {
        var button = document.getElementById('dlButton');
        button.textContent = '✔️';
        button.title = 'Done!';
    });
}

async function addFullSizeUrl(photo) {
    var link = photo.querySelector('a.overlay')
    var linkSplit = link.href.split('/');
    var username = linkSplit[4];
    var photoId = linkSplit[5];
    var maxSize = await getMaxPhotoSize(username, photoId);
    var photoUrl = getPhotoUrl(username, photoId, maxSize);
    if (!photoUrl) return;
    var url = await photoUrl;

    var engagement = photo.querySelector('div.engagement');
    var dl = document.createElement('a');
    dl.className = 'engagement-item';
    dl.href = url + '?fullsize';
    var icon = document.createElement('span');
    icon.className = 'ui-icon-download';
    icon.title = maxSize.resolution;
    dl.appendChild(icon);
    engagement.appendChild(dl);
    return maxSize;
}

async function getMaxPhotoSize(username, photoId) {
    var res = await fetch(`https://www.flickr.com/photos/${username}/${photoId}/sizes/l/`, {
        redirect: 'follow',
        credentials: "include"
    });
    var html = await res.text();
    if (!html || html.length < 1) return;

    var parser = new DOMParser();
    try {
        var doc = parser.parseFromString(html, 'text/html');
        var sizeList = [...doc.querySelectorAll('.sizes-list>li>ol>li')];
        var maxSize = sizeList.pop();
        var largestLink = maxSize.querySelector('a');
        largestLink = largestLink ? largestLink.href : res.url;
        var resDescription = maxSize.textContent.replace(/\s/g, '');
        var resName = largestLink.split('/').slice(-2)[0];
        return {
            resolution: resDescription,
            url: largestLink,
            name: resName
        };
    } catch (e) {
        console.error(e);
    }
}

async function getPhotoUrl(username, photoId, maxSize) {
    // var res = await fetch(`https://www.flickr.com/photos/${username}/${photoId}/sizes/${resolution}/`, {
    var res = await fetch(maxSize.url, {
        redirect: 'follow',
        credentials: "include"
    });
    var html = await res.text();
    if (!html || html.length < 1) return;
    var parser = new DOMParser();
    try {
        var doc = parser.parseFromString(html, 'text/html');
        var fullSize = doc.querySelector('#allsizes-photo>img');
        return fullSize.src;
    } catch (e) {
        console.error(e);
    }
    return;
  //return html.match(/https:\/\/\w{2,6}\.staticflickr\.com[\d\/]+\d{11}_[a-f0-9]{10}_\w.jpg/)[1];
}



var albumEngagement = document.querySelector('.stats-container');
var separator = document.querySelector('.album-stats-separator');
var dlButton = document.createElement('span');
dlButton.className = 'ui-icon-download';
dlButton.id = 'dlButton';
dlButton.style.cssText = 'display:inline-block; cursor:pointer';
dlButton.title = 'Load large urls';
dlButton.addEventListener("click", fetchUrls, false);
albumEngagement.insertBefore(dlButton, separator);

var dlPromises = [];
