﻿// ==UserScript==
// @name           Gray non-Linköping cities
// @include        https://www.rtog.se/larmet-gar/*
// ==/UserScript==

cities = document.querySelectorAll('.alarm-municipality');

tstyle = 'color: #ddd;';
for (city of cities) {
	if (city.innerText != 'Linköping') {
		city.parentElement.setAttribute('style',tstyle);
		city.parentElement.getElementsByClassName('alarm-heading')[0].children[0].setAttribute('style', tstyle);
    
		for (entry of city.parentNode.getElementsByTagName('td')) {
			entry.setAttribute('style', 'padding-top: 0; padding-bottom: 0');
		}
	}
}
