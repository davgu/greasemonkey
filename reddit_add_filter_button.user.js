// ==UserScript==
// @name        reddit add filter button
// @namespace   none
// @description Add a button to each link to filter out that subreddit from r/all
// @include     https://www.reddit.com/r/all/
// @include     https://www.reddit.com/r/popular/
// @version     1
// @grant       none
// ==/UserScript==

document.querySelectorAll('.link').forEach(link => {
  var sub = link.querySelector('.subreddit').textContent.replace('r/', '').toLowerCase();
  var buttons = link.querySelector('.buttons');
  var filterBtn = document.createElement('li');
  var filterA = document.createElement('a');
  filterA.textContent = 'filter';
  filterA.style.cursor = 'pointer';
  filterA.onclick = function (){filter(sub)};
  filterBtn.appendChild(filterA);
  buttons.appendChild(filterBtn);
})

function filter(sub) {
  var putUrl = 'https://www.reddit.com/api/filter/user/efraim/f/all/r/'+sub;
  var data = 'model= {"name":"'+sub+'"}';
  var headers = new Headers();
  headers.append('Content-Type', 'application/x-www-form-urlencoded');
  headers.append('X-Requested-With', 'XMLHttpRequest');
  headers.append('X-Modhash', r.config.modhash);
  headers.append('Referer', 'https://www.reddit.com/r/all/');
  var config = {
    method: 'PUT',
    headers: headers,
    credentials: 'include',
    body: data
  }
  fetch(putUrl, config)
  .then(async (r) => {
    var text = await r.text();
    window.location.reload();
  });
  return false;
}