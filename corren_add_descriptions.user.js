// ==UserScript==
// @name         corren add descriptions
// @namespace    http://corren.se
// @version      2.2
// @description  Fetch description of headlines and and on front page
// @author       David
// @match        https://corren.se/*
// @match        https://www.corren.se/*
// @icon         https://www.google.com/s2/favicons?domain=corren.se
// @noframes
// @grant        none
// ==/UserScript==

var articleCache = {}

async function fetchDesc(id) {
    if (id in articleCache) {
        console.log('Id already in cache: ', id)
        return articleCache[id]
    }
    var r = await fetch(`https://iris-api.ntm.eu/api/v1/iris/article/public/${id}`, {
        "headers": {
            "Accept": "application/json, text/plain, */*",
            "ClientName": "www.corren.se"
        },
    });
    var j = await r.json()
    articleCache[id] = j
    return j
}

async function addDesc(article) {
    if (article.parentElement.parentElement.classList.contains('horizontal-list-container')) return
    if (article.classList.contains('described')) return
    article.classList.add('described')
    var articleId = article.getAttribute('ntm-object-id') || article.href.match(/[a-z\d]+$/)[0]
    var descJ = await fetchDesc(articleId)
    try {
        var description = document.createElement('div')
        description.className = 'description-text'
        description.innerHTML = descJ.image ? `<p>${descJ.image.description}</p> <p>${descJ.preamble}</p>` : `<p>${descJ.preamble}</p>`
        var parent
        if (article.classList.contains('right-now-item') || article.classList.contains('highlight-card')) {
            parent = article
            description.style.fontSize = '12pt'
            description.style.fontWeight = 'normal'
            console.log(article)
        } else if (article.querySelector('.list-item-title')) {
            parent = article
        } else {
            parent = article.querySelector('section')
        }
        if (article.querySelector('.inverted') || article.classList.contains('highlight-card') || article.parentElement.classList.contains('dark-theme')) {
            description.style.color = '#e0e0e0'
        }
        parent.append(description)
    } catch (error) {
    }
}

function describeOnIntersection(entries, observer) {
    entries.forEach(entry => {
        if (entry.isIntersecting) {
            addDesc(entry.target)
            try {
                observer.unobserve(entry)
            } catch (e) {

            }
        }
    })
}

var lazyObserver = new IntersectionObserver(describeOnIntersection, { rootMargin: '100px' });
var mutObserver = new MutationObserver(addAllDesc)
var observerConfig = { childList: true, subtree: true }
mutObserver.observe(document.body, observerConfig)
function addAllDesc(mutations, mutObserver) {
    for (var mut of mutations) {
        for (var addedNode of mut.addedNodes) {
            try {
                addedNode.querySelectorAll('a[ntm-object-id], a[href*="/artikel"]')
                    .forEach(article => { lazyObserver.observe(article) })
            } catch (e) {
            }
        }
    }
}




