// ==UserScript==
// @name        hackernews mark high scores
// @namespace   none
// @description Show higher scores in different style
// @include     https://news.ycombinator.com/news
// @version     1
// @grant       none
// ==/UserScript==

var threshold = 200;
var scores = document.querySelectorAll('span.score');
for (var score of scores) {
  var points = parseInt(score.textContent.split(' ')[0]);
  if (points > threshold) {
    score.style.color = '#73b900';
  }
}