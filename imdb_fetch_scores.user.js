// ==UserScript==
// @name        imdb fetch scores
// @namespace   nope
// @description Fetch scores for movies in filmography
// @include     http://www.imdb.com/name/*
// @version     2
// @grant       none
// ==/UserScript==

var great = 8.0;
var good = 7.0;
var mediocre = 5.5;
var interestingRoles = ['Actor', 'Actress', 'Director', 'Writer'];
var filmos = [...document.querySelectorAll('div.filmo')]
.filter(filmo => interestingRoles.some(e => filmo.firstChild.textContent.includes(e)));
var visited = [];
var videos = [];
for (var filmo of filmos) {
  videos = videos.concat([...filmo.querySelectorAll('ol>li>a:first-child')]);
}
for (var a of videos) {
	if (visited.some(el => el.href == a.href)) {
		continue;
	}
	visited.push(a);
	fetch(a.href+'ratings', {
	  credentials: 'include',
	  headers: {
		'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'
	  }
	}).then(async (r) => {
		var text = await r.text();
	  var parser = new DOMParser();
	  var doc = parser.parseFromString(text, 'text/html');
		var score = doc.querySelector('.ratings-imdb-rating');
	  if (score == null) return;
	  score = score.dataset.value;
		var yourRating = doc.querySelector('span[name=ur]').textContent;
		var yourScore = yourRating === 'Rate' ? '' : yourRating;
	  
	  // same movie in actor, director etc
	  var titles = videos.filter(el => r.url.includes(el.href));
	  for (var title of titles) {
		  var s = createScoreNode(a, score);
		  if(s == null) return;
		  title.parentNode.insertBefore(s, title);
		  if (yourScore.length > 0) {
			var vote = document.createElement('i');
			vote.textContent = '('+yourScore+') ';
			title.parentNode.insertBefore(vote, title);
		  }
	  }
	});
}


function createScoreNode(node, score) {
	if (score.length < 1) return;
	var value = parseFloat(score);
	var s = document.createElement('i');
	var color = '#047b09';
	if (value < mediocre) {
	  color = '#cb0a0a';
	} else if (value < good) {
	  color = '#ae7c03';
	} else if (value >= great) {
	  s.style.fontWeight = 'bold';
	}
	s.textContent = ' ('+score+')';
	s.style.color = color;
	s.style.marginRight = '2px';
	s.className = 'gm-score';
	return s;
}


