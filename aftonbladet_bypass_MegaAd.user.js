// ==UserScript==
// @name        aftonbladet bypass MegaAd
// @namespace   none
// @description Bypasses initial mega ad by clicking button
// @include     http://www.aftonbladet.se/
// @version     1
// @grant       none
// ==/UserScript==

document.getElementById('MegaAdTopStreamer').click();