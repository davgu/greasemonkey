// ==UserScript==
// @name        corren open affärsliv in same tab
// @namespace   none
// @description Removes target blank from links to affärsliv
// @include     http*://corren.se/nyheter/
// @include     http*://www.corren.se/nyheter/
// @version     1.1
// @grant       none
// ==/UserScript==

var links = document.querySelectorAll('a[href^="http://www.affarsliv.com"]');
for(var link of links) {
  link.removeAttribute('target');
}