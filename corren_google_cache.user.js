// ==UserScript==
// @name         corren link to google cache
// @namespace    corren.se
// @version      2.1
// @description  Create link to google text cache
// @match        https://corren.se/bli-prenumerant/artikel/*
// @grant        none
// ==/UserScript==


setTimeout(() => {
    console.log('running cache script')
    var headline = document.querySelector('h1.headline');
    var title = encode(headline.textContent);
    var articleHash = window.location.toString().split('/').slice(-1)[0];

    var link = document.createElement('a');
    var baseUrl = 'http://webcache.googleusercontent.com/search?strip=1&q=cache:https://corren.se/asikter/artikel/';
    link.href = `${baseUrl}${title}/${articleHash}`;

    headline.parentNode.insertBefore(link, headline);
    link.appendChild(headline);
	var external = document.createElement('img');
	external.src = 'https://qsf.fs.quoracdn.net/-3-images.new_grid.external_link.svg-26-7f84ed22dfd7e97b.svg';
	external.style.marginLeft = '0.5rem';
	headline.appendChild(external);
}, 2000);


function encode(url) {
    return url.trim().replace(/[åä]/g, 'a').replace(/ö/g, 'o').replace(/\s/g, '-').toLowerCase();
}