// ==UserScript==
// @name        corren rss paywall articles
// @namespace   none
// @description Create menu from rss feeds and inserts any paywalled article in the current page
// @include     http*://www.corren.se/*.aspx
// @include     http*://corren.se/*.aspx
// @version     1.2
// @grant       none
// @run-at      document-idle
// ==/UserScript==

var feeds = [
  {'name': 'Alla nyheter', 'url': '/nyheter/rss/'},
  {'name': 'Linköping', 'url': '/nyheter/linkoping/rss/'},
  {'name': 'Mjölby', 'url': '/nyheter/mjolby/rss/'},
  {'name': 'Boxholm', 'url': '/nyheter/boxholm/rss/'},
  {'name': 'Ödeshög', 'url': '/nyheter/odeshog/rss/'},
  {'name': 'Motala', 'url': '/nyheter/motala/rss/'},
  {'name': 'Vadstena', 'url': '/nyheter/vadstena/rss/'},
  {'name': 'Åtvidaberg', 'url': '/nyheter/atvidaberg/rss/'},
  {'name': 'Kinda', 'url': '/nyheter/kinda/rss/'},
  {'name': 'Östergötland', 'url': '/nyheter/ostergotland/rss/'},
  {'name': 'Sverige', 'url': '/nyheter/sverige/rss/'},
  {'name': 'Världen', 'url': '/nyheter/varlden/rss/'}
];

var tagStyle = document.createElement('style');
tagStyle.type = 'text/css';
tagStyle.appendChild(document.createTextNode(`
  .dropdownHover:hover {
    background-color: #990810;
  }
  .rssDropdown {
    margin: auto;
    position: inherit;
    width: 80%;
  }
`));
document.head.appendChild(tagStyle);

var divDropdown = document.createElement('div');
divDropdown.classList.add('dropdown-nav');
divDropdown.classList.add('rssDropdown');
var navbar = document.createElement('div');
navbar.classList.add('navbar-items');
var dropdowns = document.createElement('ul');
dropdowns.classList.add('dropdown');

for (var feed of feeds) {
  var li = document.createElement('li');
  li.classList.add('cat2');
  li.style.cursor = 'pointer';
  li.style.height = '33px';
  var fname = document.createElement('a');
  fname.href = '#';
  fname.textContent = feed.name;
  var list = document.createElement('ul');
  list.classList.add('ddmenu-link-submenu');
  list.id = getId(feed.name);
  getRss(feed.url).then(items => {
    var rssList = document.getElementById(getId(items.feedName));
    for (var item of items.articles) {
      var tli = document.createElement('li');
      tli.classList.add('dropdownHover');
      var feedLink = document.createElement('a');
      feedLink.classList.add('ddsubmenu-link');
      feedLink.textContent = item.title;
      feedLink.href = item.link;
      feedLink.dataset.id = item.articleId;
      feedLink.dataset.name = item.title;
      feedLink.onclick = loadArticle;
      tli.appendChild(feedLink);
      rssList.appendChild(tli);
    }
  });
  li.appendChild(fname);
  li.appendChild(list);
  dropdowns.appendChild(li);
}
navbar.appendChild(dropdowns);
divDropdown.appendChild(navbar);
var artinf = document.querySelector('div.article-inf');
var jinner = artinf.querySelector('div.jscroll-inner');

artinf.insertBefore(divDropdown, jinner);

async function getRss(url) {
  var response = await fetch(window.location.protocol + '//' + window.location.host + url, {credentials: 'omit'});
  var text = await response.text();
  var parser = new DOMParser();
  var xmlDom = parser.parseFromString(text, "text/xml");
  var articleList = [...xmlDom.querySelectorAll('item')].map( i => {
    var title = i.querySelector('title').textContent;
    var link = i.querySelector('link').textContent;
    var articleId = link.match(/om\d+/)[0];
    return {'title': title, 'link': link, 'articleId': articleId};
  });
  var title = xmlDom.querySelector('title').textContent.replace('Corren.se - ', '');
  return {'feedName': title, 'articles': articleList};
}

function getId(longName) {
  return longName.replace(/\s/g, '_')
    .toLowerCase()
    .replace(/å|ä/g,'a')
    .replace('ö', 'o')
  + 'Id';
}

function loadArticle(e) {
  e.preventDefault();
  var loadDiv = document.createElement('div');
  loadDiv.style.display = 'none';
  var aNext = document.createElement('a');
  aNext.classList.add('jscroll-next');
  aNext.href = `/ws/GetArticle.ashx?articleid=${e.target.dataset.id}`;
  aNext.dataset.articleName = e.target.dataset.name;
  aNext.dataset.articleId = e.target.dataset.id;
  aNext.dataset.articleIndex = '0';
  loadDiv.appendChild(aNext);
  var jInner = document.querySelector('.jscroll-inner');
  var jParent = jInner.querySelector('.jscroll-next-parent');
  jInner.insertBefore(loadDiv, jParent);
  document.querySelectorAll('.jscroll-added').forEach(e => {
    e.parentNode.removeChild(e);
  });
  document.getElementById('sidfot-ny').scrollIntoView();
}


