// ==UserScript==
// @name        imdb hide title clutter
// @namespace   none
// @description Hide aka etc under titles
// @include     http://www.imdb.com/name/*
// @version     1
// @grant       none
// ==/UserScript==

var hoverStyle = document.createElement('style');
hoverStyle.type = 'text/css';
hoverStyle.appendChild(document.createTextNode(`
.title:hover .extra-info{
  display: block;
}
.extra-info {
  display: none;
}
`));
document.head.appendChild(hoverStyle);

var titles = document.querySelectorAll('div.filmo>ol>li')
for (title of titles) {
	title.classList.add('title')
	var extraDiv = document.createElement('div');
	var info = title.innerHTML.split('<br>');
	var extraContent = info.slice(1).join();
	title.innerHTML = info[0];
	if (extraContent.length < 4) continue;
	extraDiv.innerHTML = extraContent;
	extraDiv.classList.add('extra-info');
	var more = document.createTextNode(' [...]');
	title.appendChild(extraDiv);
	title.appendChild(more);
}
