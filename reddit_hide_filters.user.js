// ==UserScript==
// @name        reddit hide filters
// @namespace   reddit.com
// @description Hide the list of filtered subreddits on r/all
// @include     https://www.reddit.com/r/all/*
// @version     1
// @grant       none
// ==/UserScript==

var filterDetails = document.querySelector('.filtered-details');
var filterForm = filterDetails.querySelector('form');
var filterList = filterDetails.querySelector('ul');
filterDetails.insertBefore(filterForm, filterList);
var filterHeight = filterDetails.scrollHeight+50;
var filteredStyle = document.createElement('style');
filteredStyle.type = 'text/css';
filteredStyle.appendChild(document.createTextNode(`
.reddit-filter {
  overflow: hidden;
  max-height: `+ filterHeight + `px;
  transition: max-height 0.5s, ease-in-out;
}

.filter-hidden {
  max-height: 0;
}
`));
document.head.appendChild(filteredStyle);

var toggleButton = document.createElement('a');
toggleButton.text = 'Show filters';
toggleButton.className = 'morelink';
toggleButton.href = '#';
toggleButton.addEventListener('click', toggleFilters, false);
var side = document.querySelector('.side');
side.insertBefore(toggleButton, side.childNodes[2]);

filterDetails.classList.add('reddit-filter');
filterDetails.classList.toggle('filter-hidden');

function toggleFilters(e) {
  e.preventDefault();
  toggleButton.text = filterDetails.classList.contains('filter-hidden') ?
    'Hide filters' : 'Show Filters';
  filterDetails.classList.toggle('filter-hidden');
}
