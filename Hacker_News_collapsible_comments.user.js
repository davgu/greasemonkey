// ==UserScript==
// @name            Hacker News collapsible comments
// @namespace       http://twitter.com/kaixi
// @description     Reddit style collapsible comments for Hacker News
// @include         https://news.ycombinator.com/item?id=*
// @include         http://news.ycombinator.com/item?id=*
// @include         https://news.ycombinator.com/threads?*
// @include         http://news.ycombinator.com/threads?*
// @version         0.7
// ==/UserScript==

var init = function() {
    // CSS styles for the toggle button
    var style = document.createElement('style');
    style.innerHTML = '.toggleButton { cursor: pointer; font-size: 10px; margin-right: 5px; padding: 0 1px 1px; }'
                        + ' .hover { background: #828282; color: #f6f6ef; }';
    document.head.appendChild(style);

    // Find all the comments
    $('td.default').each(function() {
        $($(this).parents('tr')[1]).addClass('collapsible');
    });
    var comments = $('tr.collapsible');
    
    $('<span></span>', {
        text: '[–]',
        class: 'toggleButton'
    })
        .insertBefore('div > .comhead')
        .on('mouseenter', function() {
            $(this).addClass('hover');
        })
        .on('mouseleave', function() {
            $(this).removeClass('hover');
        })
        .on('click', function() {
            var $this = $(this),
                        comment = $this.parents('tr.collapsible'),
                        indentation = comment.find('img:first')[0].width;
            // Toggle child comments
            for (var i = comment.index('tr.collapsible') + 1; i < comments.length; ++i) {
                var nextComment = $(comments[i]);
                var nextIndentation = nextComment.find('img:first')[0].width;
                // Break if nextComment is no longer a child
                if (nextIndentation > indentation) {
                    nextComment.toggle();
                    if (nextComment.find('span.toggleButton').text() === '[+]') {
                        // Skip comments that shouldn't be toggled
                        while(i+1 < comments.length && $(comments[i+1]).find('img:first')[0].width > nextIndentation)
                            ++i;
                    }
                } else {
                    break;
                }
            }
            // Toggle current comment
            var content = comment.find('.comment').toggle().next().toggle().end();
            
            if (content.is(':visible'))
                $this.text('[–]');
            else
                $this.text('[+]');
        });
};

// Credit: http://erikvold.com/blog/index.cfm/2010/6/14/using-jquery-with-a-user-script
(function(fn) {
    var script = document.createElement('script');
    script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js';
    script.addEventListener('load', function() {
        var script = document.createElement('script');
        script.textContent = 'jQuery.noConflict();(function($){(' + fn.toString() + ')();})(jQuery);';
        document.body.appendChild(script);
    }, false);
    document.body.appendChild(script);
})(init);