// ==UserScript==
// @name         strava flyby fix
// @namespace    strava.com
// @version      1
// @description  Convert distance in list from miles to km and add description
// @match        https://labs.strava.com/flyby/viewer/*
// @grant        none
// ==/UserScript==

var descStyle = document.createElement('style');
descStyle.innerHTML = `.activity-description {
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    max-width: 15em;
    font-size: 10px;
    color: #868686;
    transform: translateY(-2px);
}`;

document.head.appendChild(descStyle);

function convertMilesToKm(activity) {
    var distance = activity.lastElementChild;
    var km = (+distance.innerText)*1.609344;
    distance.innerText = km.toFixed(0);
}

function addDescription(activity) {
    var description = document.createElement('div');
    description.className = 'activity-description';
    description.innerText = activity.title;
    activity.children[1].appendChild(description);
}

function convert() {
    observer.disconnect();
    var activities = document.querySelectorAll('tr[title]');
    activities.forEach(convertMilesToKm);
    activities.forEach(addDescription);
}

document.querySelectorAll('th.sortable')[2].title = 'Activity Distance (km)';

var activityTable = document.getElementById('activity_table');
var observer = new MutationObserver(convert);
observer.observe(activityTable, {childList: true, subtree: true});