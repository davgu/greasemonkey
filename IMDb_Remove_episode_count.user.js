// ==UserScript==
// @name        IMDb Remove episode count
// @namespace   none
// @description Removes the episode count after cast name
// @include     http://www.imdb.com/title/*
// @version     1
// @grant       none
// ==/UserScript==

var loadStyle = document.createElement('style');
loadStyle.type = 'text/css';
loadStyle.id = 'loadStyle';
loadStyle.innerHTML = 'td.char { opacity: 0;}';

document.getElementsByTagName('head')[0].appendChild(loadStyle);

document.addEventListener ("DOMContentLoaded", removeEpisodeCount);

function removeEpisodeCount () {
	var castTable = document.querySelector('table.cast');
	castTable.querySelectorAll('.char').forEach(desc => {
		var matches = desc.innerHTML.match(/\d+ episodes, /);
		if (matches) {
			desc.innerHTML = desc.innerHTML.replace(matches[0], '');
		}
		desc.style.transition = 'opacity 0.2s ease-in';
		desc.style.opacity = '1';
	});
}