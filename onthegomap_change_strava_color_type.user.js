// ==UserScript==
// @name        onthegomap change strava color type
// @namespace   onthegomap
// @include     http://onthegomap.com/*
// @include     https://onthegomap.com/*
// @version     3
// @grant       none
// @run-at      document-start
// ==/UserScript==

document.addEventListener('DOMContentLoaded', init, false);

/*
* available colors:
* hot
* blue
* bluered
* gray
*/
var bikeColor = 'blue';
var runColor = 'hot';
var opacity = '80%';


function init() {
  var mapCanvas = document.getElementById('map_canvas');
  mapCanvas.querySelectorAll('img[src^="https://heatmap-external"]').forEach(replaceTile);

  var config = {childList: true, subtree:true};
  var observer = new MutationObserver(function(mutations){
    for (var mutation of mutations) {
      for (var added of mutation.addedNodes) {
        if (/heatmap-external/.test(added.innerHTML)) {
          var stravaTile = added.querySelector('img');
          replaceTile(stravaTile);
        }
      }
    }
  });
  observer.observe(mapCanvas, config);
}


function replaceTile(tile) {
  tile.dataset.oldSrc = tile.src;
  tile.classList.add('stravaTile');
  var newSrc = tile.src;
  if (/run/.test(newSrc)) {
    var color = newSrc.match(/run\/(\w+)/)[1];
    newSrc = newSrc.replace(color, runColor);    
  } else {
    var color = newSrc.match(/ride\/(\w+)/)[1];
    newSrc = newSrc.replace(color, bikeColor);
  }
  tile.src = newSrc;
  tile.style.filter = 'opacity(' + opacity + ')';
}
