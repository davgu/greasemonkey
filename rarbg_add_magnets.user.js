// ==UserScript==
// @name        rarbg add magnets
// @namespace   none
// @description Add magnet links to torrent list
// @include     https://rarbg.to/torrents.php*
// @include     https://rarbg.to/tv/*
// @version     1
// @grant       none
// ==/UserScript==


var anchors = [...document.querySelectorAll('.lista2t a[href^="/torrent/"]')].filter(a => !/comments/.test(a));

for (var a of anchors) {
  var mag = document.createElement('a');
  mag.href = a.href;
  mag.dataset.url = a.href;
  var magIcon = new Image();
  magIcon.src = 'https://dyncdn.me/static/20/img/magnet.gif';
  magIcon.style.filter = 'grayscale(1.0)';
  mag.appendChild(magIcon);
  a.parentElement.insertBefore(mag, a.nextSibling);
  mag.addEventListener('click', fetchMagnet, true)
}

async function fetchMagnet(e) {
  e.preventDefault();
  this.removeEventListener('click', fetchMagnet, true);
  var response = await fetch(this.dataset.url, {
                    credentials: 'include',
                    headers: {
                      'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                      'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:52.0) Gecko/20100101 Firefox/52.0',
                      'Upgrade-Insecure-Requests': '1'
                    }
                  });
  var text = await response.text();
  var parser = new DOMParser();
  try {
    var doc = parser.parseFromString(text, 'text/html');
    this.href = doc.querySelector('a[href^="magnet"]').href;
    this.firstChild.style = '';
  } catch (e) {
    console.error(response.url);
  }
}

