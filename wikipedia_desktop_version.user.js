// ==UserScript==
// @name        wikipedia desktop version
// @namespace   none
// @description Navigates from mobile version to desktop version
// @include     https://*.m.wikipedia.org/wiki/*
// @version     1
// @grant       none
// @run-at      document-start
// ==/UserScript==

window.location.replace(window.location.href.replace('.m.', '.'));
