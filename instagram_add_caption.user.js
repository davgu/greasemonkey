// ==UserScript==
// @name        instagram add caption
// @namespace   none
// @description Adds the poster's caption under the image in the gallery view
// @include     https://www.instagram.com/*
// @version     1
// @grant       none
// ==/UserScript==

var itemClassName = '_70iju'; // one row
var containerSelector = 'div._cmdpi';

document.querySelectorAll('.' + itemClassName + ' a').forEach(addCaption);

var itemContainer = document.querySelector(containerSelector);
var config = {childList: true, subtree:true};
var observer = new MutationObserver(function(mutations){
  for (var mutation of mutations) {
    if (mutation.addedNodes.length < 1) continue;
    for (var addedNode of mutation.addedNodes) {
      if (addedNode.classList.contains(itemClassName)) {
        addedNode.querySelectorAll('a').forEach(addCaption);
      }
    }
  }
});
observer.observe(itemContainer, config);

function addCaption(node) {
  var image = node.querySelector('img');
  var caption = image.alt.replace(/#\S+/g, '').trim();
  var captionBox = document.createElement('div');
  var textBox = document.createTextNode(caption);
  captionBox.appendChild(textBox);
  captionBox.style.marginTop = '5px';
  node.appendChild(captionBox);
}

var article = document.querySelector('article');
article.style.maxWidth = '70%';