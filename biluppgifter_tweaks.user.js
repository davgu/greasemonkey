// ==UserScript==
// @name         Biluppgifter.se - Tweaks
// @namespace    biluppgifter.se
// @version      1
// @description  Visa ägare, göm oviktiga saker
// @match        https://biluppgifter.se/fordon/*
// @grant        GM.xmlHttpRequest
// @connect      merinfo.se
// ==/UserScript==


let owner_card = document.querySelector(".card-owner");
let merinfo_link = owner_card.querySelector(".gtm-merinfo");
let top_owner = document.querySelector('span>a[href="#owner-anchor"]').parentElement;

// Owner info
let sp = Object.fromEntries(new URLSearchParams(merinfo_link.href.split("?").pop()));
let { who: name = 'Okänd', ae6: address = 'Ingen adress', ae4: city = 'Ingen stad' } = sp;
let [last_name = 'Efternamn', first_name = 'Förnamn'] = name.split(', ');

make_owner_card(first_name, last_name, address, city);

// Show all vehicles from the same owner
var all_link = document.querySelector('a[href*="/brukare/"]');
if (all_link) {
    make_vehicles_card(all_link.href);
}

// Remove crap
remove_element(document.querySelector("section.grade"));
remove_element(document.querySelector("#box-summary div.col-auto"));

let crap_cards = ["box-lead", "box-insurance", "box-ads", "box-debt", "box-valuation", "box-loan", "box-video-ad", "biluppgiftere_pano1_6", "biluppgiftere_pano2_9"];
crap_cards.map(e => document.getElementById(e)).forEach(remove_element);


function remove_element(el) {
    if (el) {
        el.style.display = 'none';
    }
}


function make_owner_card(first_name, last_name, address, city) {
    let ssn = '-';
    let owner_info = document.createElement("div");
    let ratsit_url = `https://www.ratsit.se/sok/person?
        fnamn=${first_name}&enamn=${last_name}&gata=&postnr=&ort=&kn=&
        pnr=${ssn}&tfn=&m=1&k=1&r=1&er=1&b=1&eb=1&amin=&amax=&on=1&typ=2&page=1`.replace(/\s+/g, '');  // hehe
    let gmaps_url = `https://www.google.com/maps/search/?api=1&query=${encodeURI(address + ', ' + city)}`;
    owner_info.innerHTML = (
        `<p><strong>Ägare: </strong>
        <a id="ownerRatsit" href="${ratsit_url}">${first_name} ${last_name}</a><br>
        <strong>Adress: </strong>
        <a href="${gmaps_url}">${address}, ${city}<a><br>
        <strong>Personnummer: </strong>
        <span id="ssn">${ssn}</span></p>`);

    top_owner.appendChild(owner_info);

    // Get SSN
    GM.xmlHttpRequest({
        method: "GET",
        url: merinfo_link.href,
        onload: function (response) {
            let parser = new DOMParser();
            let doc = parser.parseFromString(response.responseText, "text/html");
            let ssn_el = doc.querySelector("p.ssn");
            if (ssn_el) {
                ssn = ssn_el.textContent.split("-")[0];
                document.getElementById('ssn').textContent = ssn;
                let ratsit = document.getElementById('ownerRatsit');
                ratsit.href = ratsit.href.replace('pnr=-', `pnr=${ssn}`);
            }
            let preferred = doc.querySelector('h2 i');
            if (preferred) {
                let preferred_name = preferred.textContent;
                let ratsit = document.getElementById('ownerRatsit');
                ratsit.innerHTML = ratsit.innerHTML.replace(preferred_name, `<strong>${preferred_name}</strong>`);
            }
        }
    });
}


// Other vehicles with the same owner
async function make_vehicles_card(all_vehicles_url) {
    let response = await fetch(all_vehicles_url);
    let html = await response.text();
    let parser = new DOMParser();
    let doc = parser.parseFromString(html, "text/html");

    let title = doc.querySelector("h1.card-title");
    if (title) {
        title.textContent = "Alla fordon från samma ägare";
    }
    let all_card = doc.querySelector("main.container div.row.mb-4");
    let owner_box = document.querySelector("#box-owner");
    owner_box.insertAdjacentElement("afterend", all_card.cloneNode(true));
}
