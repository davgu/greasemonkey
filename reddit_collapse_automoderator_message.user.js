// ==UserScript==
// @name        reddit collapse automoderator message
// @namespace   none
// @description Collapse automoderator's message to be polite in r/politics
// @include     https://www.reddit.com/r/*/comments/*
// @version     1
// @grant       none
// ==/UserScript==

collapseStickie('AutoModerator', '.stickied');
collapseStickie('PCMRBot', '.stickied', 'Before commenting');

function collapseStickie(bot, selector, message) {
	var post = document.querySelector(selector);
	var usertext = post.querySelector('.usertext-body');
	if (message) {
		var messageExp = new RegExp(message, 'i');
		if (post.dataset.author == bot && messageExp.test(usertext.innerHTML)){
		  post.className = post.className.replace('noncollapsed', 'collapsed');
		}
	} else {
		if (post.dataset.author == bot){
		  post.className = post.className.replace('noncollapsed', 'collapsed');
		}
	}
}
