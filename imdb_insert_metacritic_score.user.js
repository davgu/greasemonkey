// ==UserScript==
// @name        imdb insert metacritic score
// @namespace   none
// @description Inserts metacritic score
// @include     http://www.imdb.com/title/*
// @version     1
// @grant       GM_xmlhttpRequest
// ==/UserScript==

var title = document.getElementById('tn15title').querySelector('h1').childNodes[0].textContent.trim();

GM_xmlhttpRequest({
  method: "GET",
  url: `http://www.metacritic.com/search/all/${title}/results`,
  onload: response => {
    var parser = new DOMParser();
    var MCDom = parser.parseFromString(response.responseText, "text/html");
    var firstResult = MCDom.querySelector('li.first_result');
    var score = firstResult.querySelector('.metascore_w').textContent;
    var MCTitle = firstResult.querySelector('.product_title');
    var resultLink = firstResult.querySelector('a');
    var content = document.getElementById('tn15content');
    var info = content.querySelector('.info:not(.stars)');
    var scoreNode = document.createElement('div');
    var color = '#6c3';
    if (parseInt(score) < 40) {
      color = '#f00';
    } else if (parseInt(score) < 61) {
      color = '#fc3';
    }
    scoreNode.classList.add('info');
    scoreNode.innerHTML = `<h5>MetaCritic:</h5>
    <div class="info-content">
      <a href="${'http://www.metacritic.com'+resultLink.pathname}" title="${MCTitle.textContent}"
      style="color:white;background-color:${color};padding:2px;font-weight:bold;font-size:14px;vertical-align:middle;">
        ${score}
      </a>
    </div>`;
    content.insertBefore(scoreNode, info);
  }
});
