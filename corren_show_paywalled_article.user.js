// ==UserScript==
// @name        Corren visa betalartikel
// @namespace   none
// @include     https://login.ntm.eu/*
// @include     https://pren.corren.se/*
// @include     https://pren.nt.se/*
// @version     5.1
// @grant       GM_xmlhttpRequest
// @run-at document-start
// ==/UserScript==

(function() {
  var article_id = window.location.toString().match(/((?:\D{2})?\d+)\.aspx/)[1];
  var domain = 'corren.se';
  if (window.location.host == 'pren.nt.se') domain = 'nt.se';

  GM_xmlhttpRequest({
    method: "GET",
    url: `http://www.${domain}/ws/GetArticle.ashx?articleid=${article_id}`,
    onload: response => {
      document.head.innerHTML += `<link rel="Stylesheet" href="https://www.${domain}/public/less/application.less">`;

      var jq = document.createElement('script');
      jq.type = "text/javascript";
      jq.src = `https://www.${domain}/public/js/lib/jquery.js`;
      document.head.appendChild(jq);

      var appjs = document.createElement('script');
      appjs.type = "text/javascript";
      //appjs.src = `https://www.${domain}/public/js/app.m.js`;
      document.head.appendChild(appjs);
      var articleText = JSON.parse(response.responseText).d.html;
      document.body.innerHTML = '<div class="article-inf">' + articleText + '</div>';

      //document.querySelector('iframe').remove();
      var embed = document.querySelector('.live-center-embed');
      if (embed) {
          embed.innerHTML += `<a href=${embed.dataset.src}>Klicka för live-uppdatering</a>`;
          //embed.innerHTML += `<iframe src=${embed.dataset.src}></iframe>`;
      }
      document.querySelector('.swiper-loading').style.display = 'none';
      document.querySelector('.swiper-container').classList.add('swiper-container-horizontal');
      document.querySelector('.swiper-wrapper').style = 'transform: translate3d(301.5px, 0px, 0px); transition-duration: 0ms;';
      document.querySelector('.swiper-slide').style = 'margin-right: 20px;';

      var heading = document.querySelector('h1');
      if (heading) document.title = heading.textContent;
    }
  });

})();