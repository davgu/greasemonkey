// ==UserScript==
// @name        corren mark paywall articles
// @namespace   none
// @description Checks if links to articles are paywalled and marks them
// @include     http*://corren.se/nyheter/
// @include     http*://www.corren.se/nyheter/
// @version     3.1
// @grant       none
// ==/UserScript==

var lazyLinkObserver = new IntersectionObserver(articles => {
    for (var article of articles) {
        if (article.isIntersecting) {
            checkLink(article.target);
            lazyLinkObserver.unobserve(article.target);
        }
    }
}, { rootMargin: "100px" }); //start loading link before it is visible


[...document.querySelectorAll('a.blurb-link-area')]
    .filter(e => /corren\.se\/(nyheter|sport|kultur|bostad)/.test(e.href))
    .filter(e => !e.parentNode.classList.contains('fifth'))
    .forEach(e => lazyLinkObserver.observe(e));


function checkLink(link) {
    fetch(link.href, { method: 'HEAD', cache: 'force-cache' }).catch(error => {
        var text = link.querySelector('p');
        var paywall = text ? text : link;
        paywall.classList.add('payWall');
    });
}


var payStyle = document.createElement('style');
payStyle.innerHTML = `
.payWall::after {
  content: '💰';
}
`;
document.head.appendChild(payStyle);
