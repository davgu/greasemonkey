// ==UserScript==
// @name         strava add splits bargraph
// @namespace    http://www.strava.com
// @version      5
// @description  Makes the background on the splits colored according to time relative to the slowest split
// @match        https://www.strava.com/activities/*
// @grant        none
// ==/UserScript==

function graphify() {
    var splits = document.querySelector('#contents');

    var maxTime = 0;
    var minTime = Number.MAX_SAFE_INTEGER;
    for (var split of splits.childNodes) {
        var time = split.children[1].textContent.match(/\d+:\d+/)[0];
        time = time.split(':');
        var seconds = +time[0] * 60 + +time[1];
        maxTime = Math.max(maxTime, seconds);
        minTime = Math.min(minTime, seconds);
        split.dataset.seconds = seconds;
    }
    var average = document.querySelector('ul.inline-stats li:nth-of-type(3) strong');
    var avTime = average.textContent.match(/\d+:\d+/)[0];
    avTime = avTime.split(':');
    var avSeconds = +avTime[0] * 60 + +avTime[1];
    var avPercent = avSeconds / maxTime;
    avPercent = ease(avPercent) * 100
    var d = 0.5;
    for (var split of splits.childNodes) {
        var seconds = +split.dataset.seconds;
        var splitPercent = seconds / maxTime;
        splitPercent = ease(splitPercent) * 100
        split.style.background = `linear-gradient(90deg, rgba(255,255,255,0) ${avPercent}%, #93c5db ${avPercent}%, #93c5db ${avPercent + d}%, rgba(255,255,255,0) ${avPercent + d}%), linear-gradient(90deg, #d2edf9 ${splitPercent}%, #f000 ${splitPercent}%)`;
    }
}

function ease(x) {
    var minWidth
    //return x < 0.5 ? 4 * x * x : 1 - Math.pow(-2 * x + 2, 2) / 2
    return x * x
    //return x
    //return x*(1-minWidth) + minWidth;
}

var observer = new MutationObserver(() => {
    if (document.querySelector('#contents')) {
        observer.disconnect();
        graphify();
        return;
    }
});

let view = document.querySelector('#view')
observer.observe(view, { childList: true, subtree: true });