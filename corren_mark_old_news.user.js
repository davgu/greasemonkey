// ==UserScript==
// @name         corren mark old news
// @namespace    corren
// @version      1
// @description  Marks older articles
// @match        http://*.corren.se/*
// @match        https://*.corren.se/*
// @grant        none
// ==/UserScript==

var stamps = document.querySelectorAll('.blurb-stamp');

for (var stamp of stamps) {
  if( /\d{4}-\d{2}-\d{2}|\d\/\d/.test(stamp.textContent) ) {
    stamp.nextElementSibling.classList.add('old-news');
  }
}

var oldStyle = document.createElement('style');
oldStyle.innerHTML = `
.old-news::after {
  content: '📅';
}
`;
document.head.appendChild(oldStyle);