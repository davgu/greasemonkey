// ==UserScript==
// @name        youtube add link to videos
// @namespace   none
// @description Makes the avatar link to video page
// @include     https://www.youtube.com/watch?v=*
// @version     1
// @grant       none
// @run-at      document-idle
// ==/UserScript==

var avatar = document.querySelector('a.yt-user-photo');
avatar.href += '/videos';