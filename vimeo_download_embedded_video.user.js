// ==UserScript==
// @name        vimeo download embedded video
// @namespace   none
// @description Add button on embedded vimeo videos to download mp4
// @include     https://player.vimeo.com/video/*
// @version     2.1
// @grant       none
// ==/UserScript==


function addDownloadButton() {
  var vScript = document.body.querySelector('script').textContent;
  var objectText = vScript.match(/var r=([\s\S]*)\;if\(\!r\.request\)/);
  if (!objectText) { // some other version of the script
    vScript = vScript.replace(/\s/g, '');
    objectText = vScript.match(/varconfig=([\s\S]*)\;if\(!config\.request\)/);
  }
  objectText = objectText[1];
  var vJson = JSON.parse(objectText);
  var videos = vJson.request.files.progressive;
  videos.sort((a,b)=>{return b.width - a.width}).forEach(makeButton);
}

function makeButton(video) {
  var title = document.querySelector('title').text.replace(/ from ([\s\S]*) on Vimeo/, '');
  // make valid filename from title
  var filename = title + ' ' + video.quality + '.mp4';
  var button = document.createElement('a');
  button.href = video.url;
  button.target = '_blank';
  button.download = filename;
  button.text = video.quality;
  button.title = "Download " + video.quality;
  button.setAttribute( 'class', "button dwnld");
  button.setAttribute( 'style', 'display: inline-block; font-size: 1em; margin: 0 0 0 0.4em; color: #fff' );

  // apply mouseover effect
  button.onmouseenter = function() { button.style.color = 'rgb(68,187,255)'; };
  button.onmouseleave = function() { button.style.color = '#fff'; };

  // find control bar and add button
	var playbar = document.querySelector('.play-bar');
  playbar.insertBefore(button, playbar.querySelector('button.fullscreen'));
}

setTimeout(addDownloadButton, 1500);
